# Author Jan P Buchmann <jpb@members.fsf.org>
# Dockerfile for a simple server

FROM alpine:3.11

# Set up minimal system and create user
ARG user=wharfie
RUN adduser --gecos "" --disabled-password ${user}

# setuid for ping
RUN chmod 4755 /bin/ping
# Set up as user
USER ${user}
WORKDIR /home/${user}
RUN mkdir scripts
COPY --chown=${user}:${user} ./scripts/ scripts/

#RUN source scripts/utils.sh

ENTRYPOINT ["/bin/ash"]

#LABEL usage="default: \
#              docker run  -p 3141:3000 chlg2 => http://localhost:3141 \
#            adjust:   \
#              docker run  PORT=4000 -p 2718:4000 chlg2 => http://localhost:2718"
