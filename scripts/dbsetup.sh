#!/bin/bash
#-------------------------------------------------------------------------------
# author: Jan Piotr Buchmann <jpb@members.fsf.org>
# copyright 2020
# licence: GPL-v3.0
#-------------------------------------------------------------------------------

set -o errexit -o pipefail -o noclobber -o nounset

function dbsetup()
{
 sqlite3 test.db 'CREATE TABLE IF NOT EXISTS test (id INTEGER PRIMARY KEY,
                                                   constant TEXT,
                                                   value float);'
}
