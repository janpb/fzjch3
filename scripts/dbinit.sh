#!/bin/bash
#  -------------------------------------------------------------------------------
#  \author Jan Piotr Buchmann <jpb@members.fsf.org>
#  \copyright 2020
#  -------------------------------------------------------------------------------


set -o errexit -o pipefail -o noclobber -o nounset

MARIADB_RANDOM_ROOT_PASSWORD: "true"
