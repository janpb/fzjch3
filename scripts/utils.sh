#!/bin/bash
#-------------------------------------------------------------------------------
# author: Jan Piotr Buchmann <jpb@members.fsf.org>
# copyright 2020
# licence: GPL-v3.0
#-------------------------------------------------------------------------------

set -o errexit -o pipefail -o noclobber -o nounset

ping=$(which ping)
ssh=$(which ssh)

#Checks if container running the database can be reached
#$1 address of server
function chkconn()
{
  echo "3232321321"
  $ping -nqc3 "$1" > /dev/null && printf "## %s: available\n" "$1"
  #$ping -nqc3 "$1" > /dev/null
}

function cleanup()
{
  printf "Could not connect to %s. Ping exited with  %d\n" "$1" "$?"
}

##$1 connect to database
#function chkdb()
#{
  ##ssh
#}

"$@"
