# Author Jan P Buchmann <jpb@members.fsf.org>
# Dockerfile for a SQLite database

FROM mariadb/server:10.3 as dba

COPY scripts/dbinit.sh .
